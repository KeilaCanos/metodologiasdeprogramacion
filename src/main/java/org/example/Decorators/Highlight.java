package org.example.Decorators;

import org.example.Clases.Alumno;
import org.example.ClasesAbstractas.AlumnoDecorator;

public class Highlight extends AlumnoDecorator
{
    public Highlight(Alumno alumnoDecorado) {
        super(alumnoDecorado);
    }

    @Override
    public String mostrarCalificacion(){
        System.out.println("************************************************************");
        String calificacion = String.valueOf(getCalificacion());
        System.out.println("**      "+ super.mostrarCalificacion() + "      **");
        System.out.println("************************************************************");
        return calificacion;
    }

}
