package org.example.Decorators;

import org.example.Clases.Alumno;
import org.example.ClasesAbstractas.AlumnoDecorator;

public class ShowLegajo extends AlumnoDecorator
{
    public ShowLegajo(Alumno alumnoDecorado) {
        super(alumnoDecorado);
    }

    @Override
    public String mostrarCalificacion(){
        return super.mostrarCalificacion() + " Legajo nro " + super.getLegajo();
    }


}
