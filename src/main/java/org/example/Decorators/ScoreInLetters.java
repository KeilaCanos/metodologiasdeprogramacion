package org.example.Decorators;

import org.example.Clases.Alumno;
import org.example.ClasesAbstractas.AlumnoDecorator;

public class ScoreInLetters extends AlumnoDecorator
{
    public ScoreInLetters(Alumno alumnoDecorado) {
        super(alumnoDecorado);
    }

    @Override
    public String mostrarCalificacion(){
        String calificacion = null;
        switch (super.getCalificacion()){
            case 0 :
                calificacion = "CERO";
                break;
            case 1 :
                calificacion = "UNO";
                break;
            case 2 :
                calificacion = "DOS";
                break;
            case 3 :
                calificacion = "TRES";
                break;
            case 4 :
                calificacion = "CUATRO";
                break;
            case 5 :
                calificacion = "CINCO";
                break;
            case 6 :
                calificacion = "SEIS";
                break;
            case 7 :
                calificacion = "SIETE";
                break;
            case 8 :
                calificacion = "OCHO";
                break;
            case 9 :
                calificacion = "NUEVE";
                break;
            case 10 :
                calificacion = "DIEZ";
                break;
        }
        return super.mostrarCalificacion() + " (" + calificacion + ")";
    }


}
