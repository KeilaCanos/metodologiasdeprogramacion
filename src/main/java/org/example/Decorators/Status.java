package org.example.Decorators;

import org.example.Clases.Alumno;
import org.example.ClasesAbstractas.AlumnoDecorator;

public class Status extends AlumnoDecorator
{
    public Status(Alumno alumnoDecorado) {
        super(alumnoDecorado);
    }

    @Override
    public String mostrarCalificacion(){
        if (super.getCalificacion() >=0 && super.getCalificacion() < 4){
            return super.mostrarCalificacion() + "(DESAPROBADO)";
        }
        else if (super.getCalificacion() >=4 && super.getCalificacion() < 7) {
            return super.mostrarCalificacion() + "(APROBADO)";
        }
        else if (super.getCalificacion() >=7 && super.getCalificacion() <= 10){
            return super.mostrarCalificacion() + "(PROMOCION)";}
        else
            return "Hubo un error y no se pudo mostrar la calificacion";
    }


}
