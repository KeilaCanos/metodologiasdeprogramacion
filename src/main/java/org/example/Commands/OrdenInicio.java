package org.example.Commands;

import org.example.Clases.Aula;
import org.example.Interfaces.OrdenAula1;

public class OrdenInicio implements OrdenAula1
{
    Aula aula;

    public OrdenInicio(Aula aula){
        this.aula = aula;
    }

    public OrdenInicio(){}

    @Override
    public void ejecutar() {
        this.aula.comenzar();
    }
}
