package org.example.Commands;

import org.example.Clases.Alumno;
import org.example.Clases.Aula;
import org.example.Interfaces.IComparable;
import org.example.Interfaces.OrdenAula2;

public class OrdenLlegaAlumno implements OrdenAula2
{

    Aula aula;

    public OrdenLlegaAlumno(Aula aula){
        this.aula = aula;
    }

    public OrdenLlegaAlumno(){}

    @Override
    public void ejecutar(IComparable c) {
        Alumno alu = (Alumno) c;
        aula.nuevoAlumno(alu);
    }
}
