package org.example.Commands;

import org.example.Clases.Aula;
import org.example.Interfaces.OrdenAula1;

public class OrdenAulaLlena implements OrdenAula1
{
    Aula aula;

    public OrdenAulaLlena(Aula aula){
        this.aula = aula;
    }

    public OrdenAulaLlena(){}


    @Override
    public void ejecutar() {
        this.aula.claseLista();
    }
}
