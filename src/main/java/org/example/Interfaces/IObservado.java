package org.example.Interfaces;

import java.util.ArrayList;
import java.util.List;

public interface IObservado
{
    List<IObservador> OBSERVADORES = new ArrayList<>();

    abstract void agregarObs(IObservador obs);

    abstract void eliminarObs(IObservador obs);

    abstract void notificar(boolean vof);
}
