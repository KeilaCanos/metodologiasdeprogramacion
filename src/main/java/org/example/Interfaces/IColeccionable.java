package org.example.Interfaces;
public interface IColeccionable
{
    abstract int cuantos();
    abstract IComparable minimo();
    abstract IComparable maximo();
    abstract void agregar(IComparable c);
    abstract boolean contiene(IComparable c);

}
