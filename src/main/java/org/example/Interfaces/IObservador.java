package org.example.Interfaces;

public interface IObservador
{
    abstract void actualizar(boolean vof);
}
