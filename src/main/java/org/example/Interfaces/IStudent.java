package org.example.Interfaces;

public interface IStudent
{
    String getName();
    int yourAnswerIs(int question);
    void setScore(int score);
    String showResult();
    boolean equals(IStudent IStudent);
    boolean lessThan(IStudent IStudent);
    boolean greaterThan(IStudent IStudent);

}
