package org.example.Interfaces;

public interface Iterador
{
    abstract void primero();
    abstract void siguiente();
    abstract boolean fin();
    IComparable actual();

}
