package org.example.Interfaces;


public interface IComparable
{
    abstract boolean sosIgual(IComparable c);
    abstract boolean sosMenorQue(IComparable c);
    abstract boolean sosMayorQue(IComparable c);
}
