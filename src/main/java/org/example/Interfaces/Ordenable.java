package org.example.Interfaces;

public interface Ordenable
{
    abstract void setOrdenInicio(OrdenAula1 oa1);
    abstract void setOrdenLlegaAlumno(OrdenAula2 oa2);
    abstract void setOrdenAulaLlena(OrdenAula1 oa1);
}
