package org.example.Interfaces;

import org.example.SuperClases.Numero;

public interface IAlumno extends IComparable {
    public void setCalificacion(int calif);
    public void setEstra(IEstrategia e);
    public String getNombre();
    public int getCalificacion();
    public Numero getLegajo();
    public Numero getPromedio();
    public String toString();
    public void prestarAtencion();
    public void distraerse();
    public int responderPregunta(int preg);
    public String mostrarCalificacion();
}
