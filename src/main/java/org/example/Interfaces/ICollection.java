package org.example.Interfaces;

public interface ICollection
{
    abstract IteratorOfStudent getIterator();
    abstract void addStudent(IStudent IStudent);
    abstract void sort();
}
