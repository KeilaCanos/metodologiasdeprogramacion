package org.example.Interfaces;

import org.example.Clases.Alumno;

public interface IEstrategia
{
    abstract boolean sosMayor(Alumno a1, Alumno a2);
    abstract boolean sosMenor(Alumno a1, Alumno a2);
    abstract boolean sosIgual(Alumno a1, Alumno a2);

}
