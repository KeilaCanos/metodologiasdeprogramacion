package org.example.Interfaces;

public interface IteratorOfStudent
{
    abstract void beginning();
    abstract boolean end();
    abstract IStudent current();
    abstract void next();
}
