package org.example.Iteradores;

import org.example.SuperClases.Cola;
import org.example.Interfaces.IComparable;
import org.example.Interfaces.Iterador;

import java.util.List;

public class IteradorCola implements Iterador
{
    int contador;
    List<IComparable> elementos;

    public IteradorCola(Cola cola){
        elementos = cola.getElementosCola();
    }

    @Override
    public void primero() {
        contador = 0;
    }

    @Override
    public void siguiente() {
        contador++;
    }

    @Override
    public boolean fin() {
        return contador >= elementos.size();
    }

    @Override
    public IComparable actual() {
        return elementos.get(contador);
    }
}
