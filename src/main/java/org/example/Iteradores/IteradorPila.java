package org.example.Iteradores;

import org.example.Interfaces.IComparable;
import org.example.Interfaces.Iterador;
import org.example.SuperClases.Pila;

import java.util.List;

public class IteradorPila implements Iterador
{
    int contador;
    List<IComparable> elementos;

    public IteradorPila(Pila pila){
        elementos = pila.getElementosPila();
    }


    @Override
    public void primero() {
        contador = 0;
    }

    @Override
    public void siguiente() {
        contador++;
    }

    @Override
    public boolean fin() {
        return contador >= elementos.size();
    }

    @Override
    public IComparable actual() {
        return elementos.get(contador);
    }
}
