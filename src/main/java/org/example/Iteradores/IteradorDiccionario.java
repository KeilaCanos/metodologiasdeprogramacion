package org.example.Iteradores;

import org.example.Clases.Clave_Valor;
import org.example.SuperClases.Diccionario;
import org.example.Interfaces.IComparable;
import org.example.Interfaces.Iterador;

import java.util.List;

public class IteradorDiccionario implements Iterador
{
    int contador;
    List<Clave_Valor> elementosDic;

    public IteradorDiccionario(Diccionario dic){
        elementosDic = dic.getLista();
    }
    @Override
    public void primero() {
        contador = 0;
    }

    @Override
    public void siguiente() {
        contador++;
    }

    @Override
    public boolean fin() {
        return contador >= elementosDic.size();
    }

    @Override
    public IComparable actual() {
        return (IComparable) elementosDic.get(contador).getValor();
    }
}
