package org.example.Iteradores;

import org.example.Interfaces.IteratorOfStudent;
import org.example.Interfaces.IStudent;

import java.util.ArrayList;
import java.util.List;

public class ListOfStudentIterator implements IteratorOfStudent
{
    List<IStudent> listIStudents = new ArrayList<>();
    int index;

    public ListOfStudentIterator(List<IStudent> list){
        this.listIStudents = list;
    }

    @Override
    public void beginning() {
        index = 0;
    }

    @Override
    public boolean end() {
        return index >= listIStudents.size();
    }

    @Override
    public IStudent current() {
        return listIStudents.get(index);
    }

    @Override
    public void next() {
        index++;
    }
}
