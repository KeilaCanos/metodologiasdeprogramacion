package org.example.Iteradores;

import org.example.SuperClases.Conjunto;
import org.example.Interfaces.IComparable;
import org.example.Interfaces.Iterador;

import java.util.List;

public class IteradorConjunto implements Iterador
{
    int contador;
    List<IComparable> elementos;

    public IteradorConjunto(Conjunto conju){
        elementos = conju.getListaConjuntos();
    }

    @Override
    public void primero() {
        contador = 0;

    }

    @Override
    public void siguiente() {
        contador++;
    }

    @Override
    public boolean fin() {
        return contador >= elementos.size();
    }

    @Override
    public IComparable actual() {
        return (IComparable) elementos.get(contador);
    }
}
