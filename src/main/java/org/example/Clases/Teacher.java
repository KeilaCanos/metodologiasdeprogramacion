package org.example.Clases;

import org.example.Clases.ListOfStudent;
import org.example.Interfaces.ICollection;
import org.example.Interfaces.IteratorOfStudent;
import org.example.Interfaces.IStudent;

public class Teacher
{
    private ICollection students;

    public Teacher()
    {
        students = new ListOfStudent();
    }

    public void setStudents(ICollection _students)
    {
        students = _students;
    }

    public void goToClass(IStudent student)
    {
        students.addStudent(student);
    }

    public void teachingAClass()
    {
        IStudent student;
        IteratorOfStudent iterator = students.getIterator();

        // Pasar lista
        /*System.out.println("Good morning");
        System.out.println("I'm going to take attendance");
        iterator.beginning();
        while(! iterator.end())
        {
            student = iterator.current();
            System.out.println(student.getName() + " is present");
            iterator.next();
        }
        System.out.println("");*/

        // tomar examen
        System.out.println("I'm going to take an exam");
        iterator.beginning();
        while(! iterator.end())
        {
            student = iterator.current();
            takeAnExam(student);
            iterator.next();
        }
        System.out.println("");

        // mostrar resultado
        /*System.out.println("Exam results");
        students.sort();
        iterator.beginning();
        while(! iterator.end())
        {
            student = iterator.current();
            System.out.println(student.showResult());
            iterator.next();
        }*/
    }

    private void takeAnExam(IStudent student){
        int hit = 0;
        for(int i = 0; i < 10; i++)
        {
            int answer = student.yourAnswerIs(i);
            if(answer == (i % 3))
                hit++;
        }
        student.setScore(hit);

        //System.out.println(student.getName() + "'s score is " + hit);
    }
}

