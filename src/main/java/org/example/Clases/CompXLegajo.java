package org.example.Clases;

import org.example.Interfaces.IEstrategia;

public class CompXLegajo implements IEstrategia
{

    @Override
    public boolean sosMayor(Alumno a1, Alumno a2) {
        if (a1.legajo.getValor() > a2.legajo.getValor()){
            //System.out.println("El legajo de "+ a1.getNombre()+ "es mayor al de "+a2.getNombre());
            return true;
        }
        return false;
    }

    @Override
    public boolean sosMenor(Alumno a1, Alumno a2) {
        if (a1.legajo.getValor() < a2.legajo.getValor()){
            //System.out.println("El legajo de "+ a1.getNombre()+ "es menor al de "+a2.getNombre());
            return true;
        }
        return false;
    }

    @Override
    public boolean sosIgual(Alumno a1, Alumno a2) {
        if (a1.legajo.getValor() == a2.legajo.getValor()){
            //System.out.println("El legajo de "+ a1.getNombre()+ "es igual al de "+a2.getNombre());
            return true;
        }
        return false;
    }
}
