package org.example.Clases;

import org.example.Adapters.AlumnoAdapter;

public class Aula
{

    Teacher teacher;

    public void comenzar(){
        teacher = new Teacher();
        System.out.println("La clase ha comenzado");
    }

    public void nuevoAlumno(Alumno alu){
        AlumnoAdapter student = new AlumnoAdapter(alu);
        teacher.goToClass(student);
    }

    public void claseLista(){
        teacher.teachingAClass();
    }
}
