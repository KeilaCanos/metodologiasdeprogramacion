package org.example.Clases;

import org.example.Interfaces.IEstrategia;

public class CompXNombre implements IEstrategia
{
    @Override
    public boolean sosMayor(Alumno a1, Alumno a2) {
        int cantA1 = a1.nombre.length();
        int cantA2 = a2.nombre.length();
        if(cantA1 > cantA2){
            return true;
        }
        return false;
    }

    @Override
    public boolean sosMenor(Alumno a1, Alumno a2) {
        int cantA1 = a1.nombre.length();
        int cantA2 = a2.nombre.length();
        if(cantA1 < cantA2){
            return true;
        }
        return false;
    }

    @Override
    public boolean sosIgual(Alumno a1, Alumno a2) {
        if (a1.nombre.equals(a2.nombre)){
            System.out.println("El nombre del Alumno 1 es igual al nombre del Alumno 2");
            return true;
        }
        return false;
    }
}
