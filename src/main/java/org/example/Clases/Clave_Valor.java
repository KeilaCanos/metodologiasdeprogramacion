package org.example.Clases;

import org.example.Interfaces.IComparable;
import org.example.SuperClases.Numero;

public class Clave_Valor implements IComparable
{
    IComparable clave;
    IComparable valor;

    public Clave_Valor(IComparable clave, IComparable valor){
        this.clave = clave;
        this.valor = valor;
    }

    public Clave_Valor(IComparable clave){
        this.clave = clave;
    }

    public IComparable getClave(){return clave;}

    public IComparable getValor(){return valor;}

    public void setClave(IComparable c){ this.clave = c; }

    public void setValor(IComparable c){
        this.valor = c;
    }

    /*public boolean sosIgual(Object c) {
        Clave_Valor cv = (Clave_Valor) c;
        if (this.clave.equals(cv.getClave())){
            return true;
        }
        return false;
    }*/

    @Override
    public boolean sosIgual(IComparable c) {
        Clave_Valor cv = (Clave_Valor) c;
        if (this.clave.equals(cv.getClave())){
            return true;
        }
        return false;

    }

    @Override
    public boolean sosMenorQue(IComparable c) {
        Numero num = (Numero) c;
        Numero numValor = (Numero)this.getValor();
        if(numValor.getValor() < num.getValor()){
            return true;
        }
        return false;
    }

    @Override
    public boolean sosMayorQue(IComparable c) {
        Numero num = (Numero) c;
        Numero numValor = (Numero)this.getValor();
        if(numValor.getValor() > num.getValor()){
            return true;
        }
        return false;

    }

    public String toString(){
        return "La clave es "+ this.getClave() +" y tiene un valor de "+ this.getValor();
    }
}
