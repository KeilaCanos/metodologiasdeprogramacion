package org.example.Clases;

import org.example.Interfaces.IComparable;
import org.example.SuperClases.Numero;


public class Persona implements IComparable
{
    String nombre;
    Numero dni;

    public Persona(String nom, Numero dni){
        this.nombre = nom;
        this.dni = dni;
    }

    public Persona() {

    }

    public String getNombre(){return nombre;}
    public Numero getDni(){return dni;}

    @Override
    public boolean sosIgual(IComparable c) {
        Numero numDni = (Numero) c;
        if (numDni.sosIgual(this.dni)){
            return true;
        }
        return false;

    }

    @Override
    public boolean sosMenorQue(IComparable c) {
        return false;
    }

    @Override
    public boolean sosMayorQue(IComparable c) {
        return false;
    }
}
