package org.example.Clases;

import org.example.SuperClases.Numero;

public class AlumnoMuyEstudioso extends Alumno
{
    public AlumnoMuyEstudioso(String nombre, Numero dni, Numero legajo, Numero promedio){
        super(nombre, dni, legajo, promedio);
    }

    public AlumnoMuyEstudioso(){
        super();
    }

    @Override
    public int responderPregunta(int pregunta){
        return pregunta % 3;
    }
}
