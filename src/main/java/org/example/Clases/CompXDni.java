package org.example.Clases;

import org.example.Interfaces.IEstrategia;

public class CompXDni implements IEstrategia
{

    @Override
    public boolean sosMayor(Alumno a1, Alumno a2) {
        if (a1.dni.getValor() > a2.dni.getValor()){
            //System.out.println("El dni de "+a1.getNombre()+ " es mayor que el de "+ a2.getNombre());
            return true;
        }
        return false;
    }

    @Override
    public boolean sosMenor(Alumno a1, Alumno a2) {
        if (a1.dni.getValor() < a2.dni.getValor()){
            //System.out.println("El dni de "+a1.getNombre()+ " es menor que el de "+ a2.getNombre());
            return true;
        }
        return false;
    }

    @Override
    public boolean sosIgual(Alumno a1, Alumno a2) {
        if (a1.dni.getValor() == a2.dni.getValor()){
            //System.out.println("El dni de "+a1.getNombre()+ " y de "+ a2.getNombre()+" son iguales");
            return true;
        }
        return false;
    }
}
