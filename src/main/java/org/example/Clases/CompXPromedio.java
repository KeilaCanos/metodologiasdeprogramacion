package org.example.Clases;

import org.example.Interfaces.IEstrategia;

public class CompXPromedio implements IEstrategia
{

    @Override
    public boolean sosMayor(Alumno a1, Alumno a2) {
        if (a1.promedio.getValor() > a2.promedio.getValor()){
            //System.out.println("El promedio de "+a1.getNombre()+ " es mayor que el de "+ a2.getNombre());
            return true;
        }
        return false;
    }

    @Override
    public boolean sosMenor(Alumno a1, Alumno a2) {
        if (a1.promedio.getValor() < a2.promedio.getValor()){
            //System.out.println("El promedio de "+a1.getNombre()+ " es menor que el de "+a2.getNombre());
            return true;
        }
        return false;
    }

    @Override
    public boolean sosIgual(Alumno a1, Alumno a2) {
        if (a1.promedio.getValor() == a2.promedio.getValor()){
            //System.out.println("El promedio de "+a1.getNombre() + " es igual al de "+a2.getNombre());
            return true;
        }
        return false;
    }
}
