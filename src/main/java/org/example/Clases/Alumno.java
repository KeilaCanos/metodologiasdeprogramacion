package org.example.Clases;

import org.example.Interfaces.IAlumno;
import org.example.Interfaces.IComparable;
import org.example.Interfaces.IEstrategia;
import org.example.Interfaces.IObservador;
import org.example.SuperClases.GeneradorDatosAleatorios;
import org.example.SuperClases.Numero;

import java.util.Random;

public class Alumno extends Persona implements IObservador, IAlumno
{
    Numero legajo;
    Numero promedio;
    IEstrategia estra;
    int calificacion;
    int nroAlumno;


    public Alumno(String nom, Numero dni, Numero lega, Numero prom) {
        super(nom, dni);
        this.legajo = lega;
        this.promedio = prom;
    }

    public Alumno() {
        super();
    }

    public void setCalificacion(int calif){
        this.calificacion = calif;
    }
    public void setEstra(IEstrategia e){
        this.estra = e;
    }
    public void setNroAlumno(int nro){this.nroAlumno = nro;}
    public int getNroAlumno(){return nroAlumno;}
    public String getNombre(){return nombre;}
    public int getCalificacion(){return calificacion;}
    public Numero getLegajo(){return legajo;}
    public Numero getPromedio(){return promedio;}


    public String toString(){
        return this.getNombre();
    }

    @Override
    public boolean sosMayorQue(IComparable c) {
        Alumno alum = (Alumno) c;
        return estra.sosMayor(this, alum);
    }

    @Override
    public boolean sosMenorQue(IComparable c) {
        Alumno alum = (Alumno) c;
        return estra.sosMenor(this, alum);
    }

    @Override
    public boolean sosIgual(IComparable c) {
        Alumno alum = (Alumno) c;
        return estra.sosIgual(this, alum);
    }

    public void prestarAtencion(){
        System.out.println("Prestando atencion");
    }

    public void distraerse(){
        String[] frasesDistraccion = {"Mirando el celular", "Dibujando en el margen de la carperta", "Tirando aviones de papel"};
        Random random = new Random();
        int cantFrases = random.nextInt(frasesDistraccion.length);
        String fraseAleatoria = frasesDistraccion[cantFrases];
        System.out.println(fraseAleatoria);
    }


    @Override
    public void actualizar(boolean vof) {
        if (vof){
            prestarAtencion();
        }
        else distraerse();
    }

    public int responderPregunta(int preg){
        GeneradorDatosAleatorios gda = new GeneradorDatosAleatorios();
        int numAzar = gda.numeroAleatorio(3);
        return numAzar;
    }

    public String mostrarCalificacion(){
        return this.getNroAlumno()+")"+ this.nombre+ " " +this.getCalificacion();
    }
}
