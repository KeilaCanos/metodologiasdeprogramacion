package org.example.Clases;

import org.example.Interfaces.ICollection;
import org.example.Interfaces.IteratorOfStudent;
import org.example.Interfaces.IStudent;
import org.example.Iteradores.ListOfStudentIterator;

import java.util.ArrayList;
import java.util.List;

public class ListOfStudent implements ICollection
{
    List<IStudent> list = new ArrayList<>();

    @Override
    public IteratorOfStudent getIterator() {
        return new ListOfStudentIterator(list);
    }

    @Override
    public void addStudent(IStudent student) {
        list.add(student);
    }

    @Override
    public void sort() {
        list.sort(new StudentComparer());
    }
}
