package org.example.Clases;

import org.example.Interfaces.IComparable;
import org.example.Interfaces.IObservado;
import org.example.Interfaces.IObservador;
import org.example.SuperClases.Numero;

import java.util.ArrayList;
import java.util.List;


public class Profesor extends Persona implements IObservado
{
    Numero antiguedad;
    List<IObservador> listaObservadores = new ArrayList<>();

    public Profesor(String nombre, Numero dni, Numero anti){
        super.nombre = nombre;
        super.dni = dni;
        this.antiguedad = anti;
    }

    @Override
    public boolean sosMayorQue(IComparable c) {
        Profesor prof = (Profesor) c;
        if(prof.antiguedad.sosMayorQue(this.antiguedad)){
            return true;
        }
        return false;
    }

    @Override
    public boolean sosMenorQue(IComparable c) {
        Profesor prof = (Profesor) c;
        if(prof.antiguedad.sosMenorQue(this.antiguedad)){
            return true;
        }
        return false;
    }

    @Override
    public boolean sosIgual(IComparable c) {
        Profesor prof = (Profesor) c;
        if(prof.antiguedad.sosIgual(this.antiguedad)){
            return true;
        }
        return false;
    }

    public String toString(){
        return this.getNombre();
    }

    public void hablarAlaClase(){
        System.out.println("Hablando de algun tema");
        notificar(true);
    }

    public void escribiendoEnelPizarron(){
        System.out.println("Escribiendo en el pizarron");
        notificar(false);
    }

    @Override
    public void agregarObs(IObservador obs) {
        listaObservadores.add(obs);
    }

    @Override
    public void eliminarObs(IObservador obs) {
        listaObservadores.remove(obs);
    }

    @Override
    public void notificar(boolean vof) {
        for(IObservador obs : listaObservadores){
            if(vof){
                obs.actualizar(true);
            }
            else obs.actualizar(false);
        }
    }
}

