package org.example.Clases;

import org.example.Interfaces.IStudent;

import java.util.Comparator;

public class StudentComparer implements Comparator<IStudent>
{
    @Override
    public int compare(IStudent s1, IStudent s2)
    {
        if (s1.equals(s2)){
            return 0;}
        else if (s1.lessThan(s2)){
            return 1;}
        else{
            return -1;
        }
    }
}
