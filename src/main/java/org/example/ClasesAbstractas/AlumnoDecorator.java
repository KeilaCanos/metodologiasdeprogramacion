package org.example.ClasesAbstractas;

import org.example.Clases.Alumno;
import org.example.SuperClases.Numero;

public abstract class AlumnoDecorator extends Alumno
{
    protected Alumno alumnoDecorado;

    public AlumnoDecorator(Alumno alumnoDecorado){
        this.alumnoDecorado = alumnoDecorado;
    }

    @Override
    public String mostrarCalificacion(){
        return this.alumnoDecorado.mostrarCalificacion();
    }

    @Override
    public int getCalificacion(){
        return this.alumnoDecorado.getCalificacion();
    }

    @Override
    public void setCalificacion(int cali){
        this.alumnoDecorado.setCalificacion(cali);
    }

    @Override
    public Numero getLegajo(){return this.alumnoDecorado.getLegajo();}
}
