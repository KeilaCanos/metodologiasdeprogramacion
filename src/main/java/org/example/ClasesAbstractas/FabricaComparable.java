package org.example.ClasesAbstractas;

import org.example.Fabricas.FabricaAlumno;
import org.example.Fabricas.FabricaAlumnoEstudioso;
import org.example.Fabricas.FabricaNumeros;
import org.example.Fabricas.FabricaProfesor;
import org.example.Interfaces.IComparable;

public abstract class FabricaComparable
{
    public static IComparable crearAlAzar(int opcion){
        FabricaComparable fabri = null;
        switch (opcion){
            case 1 :
                fabri = new FabricaNumeros();
                break;
            case 2 :
                fabri = new FabricaAlumno();
                break;
            case 3 :
                fabri = new FabricaAlumnoEstudioso();
                break;
            case 4 :
                fabri = new FabricaProfesor();
                break;
        }
        return fabri.crearAlAzar();
    }

    public abstract IComparable crearAlAzar();

    public static IComparable crearPorTeclado(int opcion){
        FabricaComparable fabri = null;
        switch (opcion){
            case 1:
                fabri = new FabricaNumeros();
                break;
            case 2:
                fabri = new FabricaAlumno();
                break;
            case 3:
                fabri = new FabricaProfesor();
                break;
        }
        return fabri.crearPorTeclado();
    }

    public abstract IComparable crearPorTeclado();
}
