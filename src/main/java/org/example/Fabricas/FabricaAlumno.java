package org.example.Fabricas;
import com.github.javafaker.Faker;
import org.example.Clases.*;
import org.example.ClasesAbstractas.FabricaComparable;
import org.example.Decorators.Highlight;
import org.example.Decorators.ScoreInLetters;
import org.example.Decorators.ShowLegajo;
import org.example.Decorators.Status;
import org.example.Interfaces.IComparable;
import org.example.SuperClases.GeneradorDatosAleatorios;
import org.example.SuperClases.Numero;

import java.util.Locale;
import java.util.Scanner;

public class FabricaAlumno extends FabricaComparable
{
    public IComparable crearAlAzar(){
        Faker faker = new Faker(new Locale("es"));
        GeneradorDatosAleatorios dni = new GeneradorDatosAleatorios();
        GeneradorDatosAleatorios legajo = new GeneradorDatosAleatorios();
        GeneradorDatosAleatorios promedio = new GeneradorDatosAleatorios();
        String nombre = faker.name().firstName();
        Alumno alu = new Alumno(nombre, new Numero(dni.numeroAleatorio(50000000)), new Numero(legajo.numeroAleatorio(100)), new Numero(promedio.numeroAleatorio(10)));
        alu.setEstra(new CompXPromedio());
        //alu.setNroAlumno(nroAlumno);
        alu = new ShowLegajo(alu);
        alu = new ScoreInLetters(alu);
        alu = new Status(alu);
        alu = new Highlight(alu);
        return alu;
    }

    @Override
    public IComparable crearPorTeclado() {
        Scanner scann = new Scanner(System.in);
        System.out.println("Ingrese el nombre: ");
        String nombre = scann.next();
        System.out.println("Ingrese el numero de dni: ");
        Numero dni = new Numero(scann.nextInt());
        System.out.println("Ingrese el numero de legajo: ");
        Numero legajo = new Numero(scann.nextInt());
        System.out.println("Ingrese el promedio: ");
        Numero promedio = new Numero(scann.nextInt());
        Alumno alu = new Alumno(nombre, dni, legajo, promedio);
        return alu;
    }


}
