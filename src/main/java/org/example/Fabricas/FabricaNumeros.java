package org.example.Fabricas;
import org.example.SuperClases.GeneradorDatosAleatorios;
import org.example.SuperClases.Numero;
import org.example.ClasesAbstractas.FabricaComparable;
import org.example.Interfaces.IComparable;

import java.util.Scanner;

public class FabricaNumeros extends FabricaComparable
{
    @Override
    public IComparable crearAlAzar() {
        GeneradorDatosAleatorios numero = new GeneradorDatosAleatorios();
        Numero num = new Numero(numero.numeroAleatorio(100));
        return num;
    }

    @Override
    public IComparable crearPorTeclado() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Ingrese un numero: ");
        Numero num = new Numero(scan.nextInt());
        return num;
    }
}
