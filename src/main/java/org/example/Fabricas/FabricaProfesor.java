package org.example.Fabricas;

import com.github.javafaker.Faker;
import org.example.Clases.*;
import org.example.ClasesAbstractas.FabricaComparable;
import org.example.Interfaces.IComparable;
import org.example.SuperClases.GeneradorDatosAleatorios;
import org.example.SuperClases.Numero;

import java.util.Locale;
import java.util.Scanner;

public class FabricaProfesor extends FabricaComparable
{
    @Override
    public IComparable crearAlAzar()
    {
        Faker faker = new Faker(new Locale("es"));
        GeneradorDatosAleatorios dni = new GeneradorDatosAleatorios();
        GeneradorDatosAleatorios antiguedad = new GeneradorDatosAleatorios();
        String nombre = faker.name().firstName();
        Profesor profe = new Profesor(nombre, new Numero(dni.numeroAleatorio(50000000)), new Numero(antiguedad.numeroAleatorio(50)));
        return profe;
    }

    @Override
    public IComparable crearPorTeclado() {
        Scanner scann = new Scanner(System.in);
        System.out.println("Ingrese el nombre: ");
        String nombre = scann.next();
        System.out.println("Ingrese el dni: ");
        Numero dni = new Numero(scann.nextInt());
        System.out.println("Ingrese los anios de antiguedad: ");
        Numero antiguedad = new Numero(scann.nextInt());
        Profesor profe = new Profesor(nombre, dni, antiguedad);
        return profe;
    }
}
