package org.example;

import org.example.Adapters.AlumnoAdapter;
import org.example.Clases.Alumno;
import org.example.Clases.Aula;
import org.example.Clases.ListOfStudent;
import org.example.Clases.Teacher;
import org.example.Commands.OrdenAulaLlena;
import org.example.Commands.OrdenInicio;
import org.example.Commands.OrdenLlegaAlumno;
import org.example.Proxy.AlumnoProxy;
import org.example.SuperClases.Pila;
import org.example.SuperClases.ServicioColeccionable;

public class Main {
    public static void main(String[] args)
    {
        /*Teacher teacher = new Teacher();
        ListOfStudent list = new ListOfStudent();
        ServicioColeccionable.createStudents(list);
        ServicioColeccionable.takeClass(teacher, list);
        teacher.teachingAClass();*/
        Pila p1 = new Pila();
        Aula a1 = new Aula();
        p1.setOrdenInicio(new OrdenInicio(a1));
        p1.setOrdenLlegaAlumno(new OrdenLlegaAlumno(a1));
        p1.setOrdenAulaLlena(new OrdenAulaLlena(a1));
        ServicioColeccionable.llenar(p1, 2);
        ServicioColeccionable.llenar(p1, 3);

    }
}



