package org.example.Proxy;

import org.example.Clases.Alumno;
import org.example.Fabricas.FabricaAlumno;



public class AlumnoProxy extends Alumno
{
    Alumno alumnoReal = null;
    String nombre;

    @Override
    public String getNombre(){
        return nombre;
    }

    @Override
    public int responderPregunta(int preg){
        if(alumnoReal == null){
            System.out.println("Se creo el alumno real con exito");
            alumnoReal = (Alumno) new FabricaAlumno().crearAlAzar(2);

        }

        return alumnoReal.responderPregunta(preg);
    }
}
