package org.example.Proxy;

import org.example.Clases.Alumno;
import org.example.Clases.AlumnoMuyEstudioso;
import org.example.Fabricas.FabricaAlumno;


public class AluEstudiosoProxy extends Alumno
{

    Alumno aluEstudiosoReal = null;

    @Override
    public int responderPregunta(int preg){
        if(aluEstudiosoReal == null){
            System.out.println("Se creo el alumno estudioso real con exito");
            aluEstudiosoReal = (Alumno) new FabricaAlumno().crearAlAzar(3);

        }

        return aluEstudiosoReal.responderPregunta(preg);
    }



}
