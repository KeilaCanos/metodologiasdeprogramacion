package org.example.SuperClases;

import org.example.Interfaces.*;
import org.example.Interfaces.Iterable;
import org.example.Iteradores.IteradorPila;

import java.util.ArrayList;
import java.util.List;

public class Pila implements IColeccionable, Iterable, Ordenable
{
    List<IComparable> elementosPila = new ArrayList<>();
    OrdenAula1 ordenInicio;
    OrdenAula2 ordenLlegada;
    OrdenAula1 ordenFin;

    public List<IComparable> getElementosPila(){
        return elementosPila;
    }


    public void push (IComparable c)
    {
        elementosPila.add(c);
        if (cuantos() == 1){
            ordenInicio.ejecutar();
        }

        ordenLlegada.ejecutar(c);

        if (cuantos() == 40){
            ordenFin.ejecutar();
        }

    }

    public IComparable pop()
    {
        IComparable c = elementosPila.get(elementosPila.size() - 1);
        elementosPila.remove(c);
        return c;
    }

    public int cuantos()
    {
        return elementosPila.size();
    }

    public void agregar(IComparable c) {
        push(c);
    }

    public IComparable minimo() {
        IComparable min = elementosPila.get(0);
        for (IComparable c : elementosPila){
            //Alumno a = (Alumno) c;
            if (c.sosMenorQue(min)){
                min = c;
            }
        }

        return min;
    }

    public IComparable maximo(){
        IComparable max = elementosPila.get(0);
        for (IComparable c : elementosPila){
            //Alumno a = (Alumno) c;
            if (c.sosMayorQue(max)){
                max = c;
            }
        }

        return max;
    }

    public boolean contiene(IComparable c)
    {
        boolean esta = false;
        for (IComparable comp : elementosPila){
            //Alumno a = (Alumno) c;
            if (comp.sosIgual(c)){
                esta = true;
                break;
            }

        }
        return esta;
    }

    @Override
    public Iterador crearIterador() {
        return new IteradorPila(this);
    }

    @Override
    public void setOrdenInicio(OrdenAula1 oa1) {
        this.ordenInicio = oa1;
    }

    @Override
    public void setOrdenLlegaAlumno(OrdenAula2 oa2) {
        this.ordenLlegada = oa2;
    }

    @Override
    public void setOrdenAulaLlena(OrdenAula1 oa1) {
        this.ordenFin = oa1;
    }
}
