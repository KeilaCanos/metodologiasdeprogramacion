package org.example.SuperClases;

import org.example.Clases.Clave_Valor;
import org.example.Interfaces.*;
import org.example.Interfaces.Iterable;
import org.example.Iteradores.IteradorDiccionario;


import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Diccionario implements IColeccionable, Iterable, Ordenable
{
    List<Clave_Valor> lista = new ArrayList<>();
    OrdenAula1 ordenInicio;
    OrdenAula2 ordenLlegada;
    OrdenAula1 ordenFin;

    public List<Clave_Valor> getLista(){
        return lista;
    }


    public Object getValorClave(Clave_Valor cla){
        for (Clave_Valor cv : lista){
            if(cla.sosIgual(cv)){
                return cla.getValor();
            }
        }
        return null;
    }

    @Override
    public int cuantos() {
        return lista.size();
    }

    @Override
    public IComparable minimo() {
        IComparable valorMinimo =  lista.get(0);
        for (Clave_Valor comp : lista){
            if (comp.sosMenorQue(valorMinimo)){
                valorMinimo = comp;
            }
        }
        return valorMinimo;
    }

    @Override
    public IComparable maximo() {
        IComparable valorMaximo =  lista.get(0);
        for (Clave_Valor comp : lista){
            if (comp.sosMayorQue(valorMaximo)){
                valorMaximo = comp;
            }
        }
        return valorMaximo;
    }

    @Override
    public void agregar(IComparable c) {
        Numero clave = crearClave();
        agregar(clave, c);
        if (cuantos() == 1){
            ordenInicio.ejecutar();
        }

        ordenLlegada.ejecutar(c);

        if (cuantos() == 40){
            ordenFin.ejecutar();
        }
    }

    public void agregar(IComparable cla, IComparable val) {
        Clave_Valor cv = new Clave_Valor(cla, val);
        if (lista.size() == 0){
            lista.add(cv);
        }
        else {
            for (Clave_Valor c : lista){
                if (c.sosIgual(cla)){
                    c.setValor(val);
                }
                else {
                    lista.add(cv);
                    break;
                }
            }
        }

    }

    @Override
    public boolean contiene(IComparable c) {
        for (Clave_Valor comp : lista){
            if (comp.getValor().equals(c)){
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterador crearIterador() {
        return new IteradorDiccionario(this);
    }

    public Numero crearClave(){
        Random random = new Random();
        Numero clave = new Numero(random.nextInt());
        return clave;

    }

    @Override
    public void setOrdenInicio(OrdenAula1 oa1) {
        this.ordenFin = oa1;
    }

    @Override
    public void setOrdenLlegaAlumno(OrdenAula2 oa2) {
        this.ordenLlegada = oa2;
    }

    @Override
    public void setOrdenAulaLlena(OrdenAula1 oa1) {
        this.ordenFin = oa1;
    }
}
