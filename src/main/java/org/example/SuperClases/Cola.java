package org.example.SuperClases;


import org.example.Interfaces.*;
import org.example.Interfaces.Iterable;
import org.example.Iteradores.IteradorCola;

import java.util.ArrayList;
import java.util.List;

public class Cola implements IColeccionable, Iterable, Ordenable
{
    List<IComparable> elementosCola = new ArrayList<>();
    OrdenAula1 ordenInicio;
    OrdenAula2 ordenLlegada;
    OrdenAula1 ordenFin;


    public List<IComparable> getElementosCola() {
        return elementosCola;
    }

    public void enCola(IComparable c)
    {
        elementosCola.add(c);
        if (cuantos() == 1){
            ordenInicio.ejecutar();
        }

        ordenLlegada.ejecutar(c);

        if (cuantos() == 40){
            ordenFin.ejecutar();
        }
    }

    public IComparable deCola()
    {
        IComparable primElemen = elementosCola.get(0);
        elementosCola.remove(primElemen);
        return primElemen;
    }


    @Override
    public int cuantos() {
        return elementosCola.size();
    }

    @Override
    public IComparable minimo()
    {
        IComparable elemMinimo = elementosCola.get(0);
        for (IComparable c : elementosCola){
            if (c.sosMenorQue(elemMinimo)){
                elemMinimo = c;
            }
        }
        return elemMinimo;
    }

    @Override
    public IComparable maximo()
    {
        IComparable elemMaximo = elementosCola.get(0);
        for (IComparable c : elementosCola){
            if (c.sosMayorQue(elemMaximo)){
                elemMaximo = c;
            }
        }
        return elemMaximo;
    }

    @Override
    public void agregar(IComparable c) {
        enCola(c);
    }

    @Override
    public boolean contiene(IComparable c)
    {
        boolean esta = false;
        for (IComparable comp : elementosCola){
           if (comp.sosIgual(c)){
               esta = true;
               break;
           }

        }
        return esta;
    }

    @Override
    public Iterador crearIterador() {
        return new IteradorCola(this);
    }

    @Override
    public String toString(){
        return "Elementos de cola";
    }

    @Override
    public void setOrdenInicio(OrdenAula1 oa1) {
        this.ordenInicio = oa1;
    }

    @Override
    public void setOrdenLlegaAlumno(OrdenAula2 oa2) {
        this.ordenLlegada = oa2;
    }

    @Override
    public void setOrdenAulaLlena(OrdenAula1 oa1) {
        this.ordenFin = oa1;
    }
}
