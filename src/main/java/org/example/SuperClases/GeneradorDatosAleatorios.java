package org.example.SuperClases;
import java.util.Random;

public class GeneradorDatosAleatorios
{
    public int numeroAleatorio(int max)
    {
        Random random = new Random();
        int num = random.nextInt(max);
        return num;
    }

    public String stringAleatorio(int cant)
    {
       String letras = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
       Random random = new Random();
       StringBuilder creadorString = new StringBuilder();

       for(int i = 0; i<cant; i++){
           int letrasIndex = random.nextInt(letras.length());
           char letraAleatoria = letras.charAt(letrasIndex);
           creadorString.append(letraAleatoria);
        }

       return creadorString.toString();
    }
}
