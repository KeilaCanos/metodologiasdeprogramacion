package org.example.SuperClases;
import org.example.Adapters.AlumnoAdapter;
import org.example.Clases.Alumno;
import org.example.Clases.ListOfStudent;
import org.example.Clases.Teacher;
import org.example.Clases.Profesor;
import org.example.ClasesAbstractas.FabricaComparable;
import org.example.Interfaces.*;
import org.example.Interfaces.IComparable;
import org.example.Interfaces.Iterable;
import org.example.Proxy.AluEstudiosoProxy;
import org.example.Proxy.AlumnoProxy;

public class ServicioColeccionable
{
    public static void llenar(IColeccionable c, int opcion)
    {
        int contador = 1;
        while (contador <= 20) {
            IComparable fabricaComp = FabricaComparable.crearAlAzar(opcion);
            c.agregar(fabricaComp);
            contador++;
        }
    }

    /*public static void informar(IColeccionable c, int opcion)
    {
        System.out.println("El coleccionable tiene "+ c.cuantos() + " elementos");
        System.out.println("Valor minimo encontrado es de "  + c.minimo());
        System.out.println("Valor maximo encontrado es de " + c.maximo());

        IComparable fabriNueva = FabricaComparable.crearAlAzar(opcion);
        if (c.contiene(fabriNueva)){
            System.out.println("El elemento ya existe en la coleccion");
        }
        else {
            System.out.println("El elemento no existe en la coleccion");
        }
        System.out.println("---");

    }*/


    public static void imprimirElementos(IColeccionable c){
        Iterador itera = ((Iterable)c).crearIterador();
        while(itera.fin() != true){
            imprimir(itera.actual());
            itera.siguiente();
        }
    }

    public static void imprimir(IComparable c){
        System.out.println(c.toString());
    }

    public static void cambiarEstrategia(IColeccionable c, IEstrategia e){
        Iterador itera = ((Iterable)c).crearIterador();
        while(itera.fin() != true){
            Alumno alu = (Alumno) itera.actual();
            alu.setEstra(e);
            itera.siguiente();
        }
    }

    public static void dictarClases(Profesor profe){
        int contador = 1;
        while(contador <= 5){
            profe.hablarAlaClase();
            profe.escribiendoEnelPizarron();
        }
    }

    public static void createStudents(ListOfStudent list){
        int index = 1;
        while (index <= 20){
            if (index <= 10){
                AlumnoProxy ap = new AlumnoProxy();
                AlumnoAdapter student = new AlumnoAdapter(ap);
                list.addStudent(student);
                index++;
            }
            else {
                AluEstudiosoProxy aep = new AluEstudiosoProxy();
                AlumnoAdapter stu = new AlumnoAdapter(aep);
                list.addStudent(stu);
                index++;
            }
        }
    }

    public static void takeClass(Teacher teacher, ListOfStudent listStudents){
        IteratorOfStudent iterator = listStudents.getIterator();
        while (!iterator.end()){
            teacher.goToClass(iterator.current());
            iterator.next();
        }

    }

    public static void showScore(ListOfStudent listStudents){
        IteratorOfStudent iterator = listStudents.getIterator();
        while (!iterator.end()){
            iterator.current().showResult();
            iterator.next();
        }
    }

}
