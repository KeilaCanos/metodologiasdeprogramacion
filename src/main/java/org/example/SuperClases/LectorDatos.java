package org.example.SuperClases;

import java.util.Scanner;

public class LectorDatos
{
    public int leerNumero(){
        Scanner scann = new Scanner(System.in);
        int numero = scann.nextInt();
        scann.close();
        return numero;
    }

    public String leerString(){
        Scanner scann = new Scanner(System.in);
        String cadenaTexto = scann.nextLine();
        scann.close();
        return cadenaTexto;
    }
}
