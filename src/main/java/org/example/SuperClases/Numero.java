package org.example.SuperClases;

import org.example.Interfaces.IComparable;

//EJERCICIO 2
public class Numero implements IComparable
{
    int valor;

    public Numero(){}
    public Numero(int val)
    {
        this.valor = val;
    }

    public int getValor(){
        return this.valor;
    }

    public boolean sosIgual(IComparable c)
    {
        Numero num = (Numero) c;
        if (valor == num.getValor()){
            return true;
        }
        else return false;
    }

    public boolean sosMenorQue(IComparable c)
    {
        Numero num = (Numero) c;
        if (valor < num.getValor()){
            return true;
        }
        else return false;
    }

    public boolean sosMayorQue(IComparable c)
    {
        Numero num = (Numero) c;
        if (valor > num.getValor()){
            return true;
        }
        else return false;
    }

    public String toString(){
        return ""+this.getValor();
    }
}

