package org.example.SuperClases;


import org.example.Interfaces.*;
import org.example.Interfaces.Iterable;
import org.example.Iteradores.IteradorConjunto;

import java.util.ArrayList;
import java.util.List;

public class Conjunto implements IColeccionable, Iterable, Ordenable
{
    List<IComparable> listaConjuntos = new ArrayList<>();
    OrdenAula1 ordenInicio;
    OrdenAula2 ordenLlegada;
    OrdenAula1 ordenFin;

    public List<IComparable> getListaConjuntos(){
        return listaConjuntos;
    }

    @Override
    public int cuantos() {
        return listaConjuntos.size();
    }

    @Override
    public IComparable minimo() {
        return null;
    }

    @Override
    public IComparable maximo() {
        return null;
    }

    @Override
    public void agregar(IComparable c) {
        if (listaConjuntos.size() == 0){
            listaConjuntos.add(c);
        }
        else {
            for (IComparable comp : listaConjuntos){
                if (comp == c){
                    System.out.println("No es posible agregar el elemento porque ya existe en la lista");
                }
                else {
                    listaConjuntos.add(c);
                    break;
                }
            }
        }
        if (cuantos() == 1){
            ordenInicio.ejecutar();
        }

        ordenLlegada.ejecutar(c);

        if (cuantos() == 40){
            ordenFin.ejecutar();
        }
    }

    @Override
    public boolean contiene(IComparable c) {
        for (IComparable comp : listaConjuntos){
            if (comp == c){
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterador crearIterador() {
        return new IteradorConjunto(this);
    }


    @Override
    public void setOrdenInicio(OrdenAula1 oa1) {
        this.ordenInicio = oa1;
    }

    @Override
    public void setOrdenLlegaAlumno(OrdenAula2 oa2) {
        this.ordenLlegada = oa2;
    }

    @Override
    public void setOrdenAulaLlena(OrdenAula1 oa1) {
        this.ordenFin = oa1;
    }
}
