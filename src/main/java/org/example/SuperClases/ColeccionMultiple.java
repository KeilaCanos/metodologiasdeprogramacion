package org.example.SuperClases;

import org.example.Interfaces.IColeccionable;
import org.example.Interfaces.IComparable;
import org.example.SuperClases.Cola;
import org.example.SuperClases.Pila;

//EJERCICIO 8
public class ColeccionMultiple implements IColeccionable
{
    Pila pila;
    Cola cola;

    ColeccionMultiple(Pila pila, Cola cola){
        this.pila = pila;
        this.cola = cola;
    }

    @Override
    public int cuantos() {
        return pila.cuantos() + cola.cuantos();
    }

    @Override
    public IComparable minimo() {
        IComparable minimoPila = pila.minimo();
        IComparable minimoCola = cola.minimo();
        if (minimoPila.sosMenorQue(minimoCola)){
            return minimoPila;
        }
        return minimoCola;
    }

    @Override
    public IComparable maximo() {
        IComparable maximoPila = pila.maximo();
        IComparable maximoCola = cola.maximo();
        if (maximoPila.sosMayorQue(maximoCola)){
            return maximoPila;
        }
        return maximoCola;

    }

    @Override
    public void agregar(IComparable c) {

    }

    @Override
    public boolean contiene(IComparable c) {
        boolean algunoDeLosDos = false;
        boolean contPila = pila.contiene(c);
        boolean contCola = cola.contiene(c);
        if (contPila && contCola) {
            System.out.println("Dos alumnos tienen ese promedio");
            return true;
        } else if (contPila || contCola) {
            System.out.println("Al menos un alumno tiene ese promedio");
        }
        System.out.println("Ningun alumno posee ese promedio");
        return false;
    }

}
