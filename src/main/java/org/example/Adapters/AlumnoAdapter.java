package org.example.Adapters;

import org.example.Clases.Alumno;
import org.example.Interfaces.IAlumno;
import org.example.Interfaces.IStudent;

public class AlumnoAdapter implements IStudent
{
    Alumno alum;

    public AlumnoAdapter(Alumno alu){
        this.alum = alu;
    }

    public IAlumno getAlumno(){
        return this.alum;
    }


    @Override
    public String getName() {
        return alum.getNombre();
    }

    @Override
    public int yourAnswerIs(int question) {
        return alum.responderPregunta(question);
    }

    @Override
    public void setScore(int score) {
        alum.setCalificacion(score);
    }

    @Override
    public String showResult() {
        return alum.mostrarCalificacion();
    }

    @Override
    public boolean equals(IStudent student) {
        if (student instanceof AlumnoAdapter) {
            AlumnoAdapter otherAdapter = (AlumnoAdapter) student;
            return alum.sosIgual(otherAdapter.getAlumno());
        }
        return false;
    }

    @Override
    public boolean lessThan(IStudent student) {
        if (student instanceof AlumnoAdapter) {
            AlumnoAdapter otherAdapter = (AlumnoAdapter) student;
            return alum.sosMenorQue(otherAdapter.getAlumno());
        } else {
            return false;
        }
    }

    @Override
    public boolean greaterThan(IStudent student) {
        if (student instanceof AlumnoAdapter) {
            AlumnoAdapter otherAdapter = (AlumnoAdapter) student;
            return alum.sosMayorQue(otherAdapter.getAlumno());
        } else {
            return false;
        }
    }
}
